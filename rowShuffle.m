function shuffledArray = rowShuffle(orderedArray)
shuffledArray = orderedArray(randperm(size(orderedArray,1)),:);
end