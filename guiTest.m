function guiTest
%% Generate constant stimuli for unique hue. 
% Center values and range estimated by 
% 
% Shamey Tab. 3
%
% Configure global data for figure

bgRGB = lch2rgb([72, 0, 0]);

%                                         [L,    C,    h   ], n, delta
dat.yellowTestColors = generateRGBSamples([75,   75,   86.1], 2, 12);
dat.blueTestColors   = generateRGBSamples([73.5, 40.3, 250 ], 2, 26);
dat.redTestColors    = generateRGBSamples([59,   72,   25.8], 2, 25);
dat.greenTestColors  = generateRGBSamples([80,   50,   150 ], 2, 30);

dat.outputFileName = [datestr(now,30) 'lab2dat'];

% have four preset orders to display:
    % 1: YBRG
    % 2: BYGR
    % 3: RGBY
    % 4: GRYB
dat.order = randi(4);

testRepeats = 2;
switch dat.order
    case 1
        dat.tests = rowShuffle(repmat(dat.yellowTestColors,            testRepeats, 1));
        dat.tests = [dat.tests; rowShuffle(repmat(dat.blueTestColors,  testRepeats, 1))];
        dat.tests = [dat.tests; rowShuffle(repmat(dat.redTestColors,   testRepeats, 1))];
        dat.tests = [dat.tests; rowShuffle(repmat(dat.greenTestColors, testRepeats, 1))];
    
    case 2
        dat.tests =             rowShuffle(repmat(dat.blueTestColors,   testRepeats, 1));
        dat.tests = [dat.tests; rowShuffle(repmat(dat.yellowTestColors, testRepeats, 1))];
        dat.tests = [dat.tests; rowShuffle(repmat(dat.greenTestColors,  testRepeats, 1))];
        dat.tests = [dat.tests; rowShuffle(repmat(dat.redTestColors,    testRepeats, 1))];

    case 3
        dat.tests =             rowShuffle(repmat(dat.redTestColors,    testRepeats, 1));
        dat.tests = [dat.tests; rowShuffle(repmat(dat.greenTestColors,  testRepeats, 1))];
        dat.tests = [dat.tests; rowShuffle(repmat(dat.blueTestColors,   testRepeats, 1))];
        dat.tests = [dat.tests; rowShuffle(repmat(dat.yellowTestColors, testRepeats, 1))];

    case 4
        dat.tests =             rowShuffle(repmat(dat.greenTestColors,  testRepeats, 1));
        dat.tests = [dat.tests; rowShuffle(repmat(dat.redTestColors,    testRepeats, 1))];
        dat.tests = [dat.tests; rowShuffle(repmat(dat.yellowTestColors, testRepeats, 1))];
        dat.tests = [dat.tests; rowShuffle(repmat(dat.blueTestColors,   testRepeats, 1))];
end        

% initialize current index to 1 and test results to [0 0 ... 0]
dat.curColorIdx = 1;
dat.testResults = zeros(length(dat.tests), 2);

%% Construct figure
%  and set data 
close all


myFig = figure('Color', bgRGB);

myAx = axes('Position',[0 0 1 1], 'Color', bgRGB);

circSize = 0.7

dat.myStim = rectangle('Position', [-circSize/2 -circSize/2 circSize circSize], 'Curvature', 1, 'FaceColor', dat.tests(dat.curColorIdx,1:3), 'EdgeColor', 'none');
dat.promptTxt = text(0, .75, '', 'HorizontalAlignment', 'center');
dat.promptTxt.FontSize = 16;

axis off; axis equal;
myAx.YLim = [-1,1];
myAx.XLim = [-1,1];
myFig.KeyPressFcn = @myKeyCallBack;

setPrompt(dat);
guidata(myFig, dat);


end

%% Utility Functions

function myKeyCallBack(obj, eventData)
dat = guidata(obj);

% store a left arrow as 0 and a right arrow as 1
switch eventData.Key
    case 'leftarrow'
        dat.testResults(dat.curColorIdx, :) = [dat.tests(dat.curColorIdx,4), 0];
    case 'rightarrow'
        dat.testResults(dat.curColorIdx, :) = [dat.tests(dat.curColorIdx,4), 1];
end

dat.curColorIdx = dat.curColorIdx + 1;

% End of experiment, save, close figure, end program. 
if (dat.curColorIdx > length(dat.tests))
    save(dat.outputFileName, '-struct', 'dat');
    close all
    return
end

% update the circle color
set(dat.myStim, 'FaceColor', dat.tests(dat.curColorIdx,1:3));
setPrompt(dat);

guidata(obj, dat);
end


%% Deal with prompt

function setPrompt(dat)

numTests = length(dat.tests);

if(dat.order < 3 && dat.curColorIdx <= numTests / 2 ||...
   dat.order > 2 && dat.curColorIdx > numTests / 2) 
    dat.promptTxt.String = 'Is the sample more green (left) or red (right)?'
else
    dat.promptTxt.String = 'Is the sample more yellow (left) or blue (right)?'
end

end

%% References
%
% Renzo Shamey, Muhammad Zubair, and Hammad Cheema, 
% "Unique hue stimulus selection using Munsell color 
% chips under different chroma levels and 
% illumination conditions," J. Opt. Soc. Am. A 36, 
% 983-993 (2019) 