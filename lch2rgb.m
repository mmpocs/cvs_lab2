function rgb = lch2rgb(lch)
%% Needs to be updated to generate real RGB values for
%  test display. 

[a, b] = pol2cart(deg2rad(lch(:,3)), lch(:,2));

rgb = lab2rgb([lch(:,1), a, b]);

end