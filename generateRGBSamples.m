function out = generateRGBSamples(centerLCh, n, maxDeltah)
% total samples = 2n + 1
stepSize = maxDeltah / n;

% generate n hue values based on step size
hues = ((centerLCh(3) - n * stepSize):stepSize:(centerLCh(3) + n * stepSize))';

% we want n samples, so replicate the center LCH value n times, then change
% the hues column to what we just generated
LCh = repmat(centerLCh, 2 * n + 1, 1);
LCh(:,3) = hues;

% preserve the angle when converting, so we output in the form of
% [R G B angle]
out = [lch2rgb(LCh) LCh(:,3)];
end